# Homework





## Test https://vk.com for "File upload"


### Extension Validation

| Test step                                                  | Result                                                    | 
| ---------------------------------------------------------- | --------------------------------------------------------- |
| Open vk.com, log in, click "Files"                         | -                                                         |
| Click "Upload file"                                        | Upload file pop up appears                                |
| Try to upload regular .png file                            | File uploads without any errors                           |
| Try to upload any .exe file                                | "Failed to upload the file. Executable files and files over 2 GB aren't allowed." |
| Try to upload .png.exe file                                | "Failed to upload the file. Executable files and files over 2 GB aren't allowed." |
| Try to upload .exe.png file                                | File uploads without any errors                           |
| Try to upload .exe%00.jpg                                  | File is uploaded, but name is not truncated               |

Conclusion: VK extention validation works fine. It implements blacklist of extentions, and it does not break in scenarios described above


### File Content Validation

| Test step                                                  | Result                                                    | 
| ---------------------------------------------------------- | --------------------------------------------------------  |
| Open vk.com, log in, click "Files"                         | -                                                         |
| Click "Upload file"                                        | Upload file pop up appears                                |
| Try to upload any .exe file                                | "Failed to upload the file. Executable files and files over 2 GB aren't allowed." |
| Put .exe file into zip folder and try to upload it         | "The archive contains an invalid file and cannot be uploaded to the Documents section." |
| Rename file to change extention from app.exe to app.png    | File uploads without any error and can be downloaded      |

Conclusion: VK file content validation does not always work. VK checks only file extention. VK checks for content in case of archives, but again, it checks only filenames. It does not check that user tries to upload .png file, which is clearly not an image





## Test https://kazanexpress.ru/ for "Forgot Password"

Here I decided to test for all points in the OWASP "Forgot Password" checklist.

| Test step                                                  | Result                                                    | 
| ---------------------------------------------------------- | --------------------------------------------------------- |
| Open kazanexpress.ru, log in, click "forgot password"      | -                                                         |
| Enter non-existing account email                           | "This account is not found" message appears               |
| Enter existing account email                               | 6-digit code is sent to email and popup window to enter it appears |
| Try to change password multiple times with the same code   | The code is not working the second time                   |
| Request password change, wait 5 minutes, try to proceed    | The code is not working after 5 minutes                   |
| Request password change, try to bruteforce code            | There is a timeout after 10th attempt                     |

Conclusion: it's pretty good, almost all points are taken into account. Codes are one-use only, expire pretty fast, sent via side channel and no modifications made to the account until the correct code is inputed. However, there is inconsistent message between existent and non-existent accounts. That makes it possible to determine is person is registered on this particular website. I did not test for consistent time, because "inconsistent message" already allows to do the same.
